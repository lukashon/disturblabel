PyTorch implementation of [DisturbLabel: Regularizing CNN on the Loss Layer](https://arxiv.org/abs/1605.00055) [CVPR 2016] extended with Directional DisturbLabel method.

This project is built on top of  https://github.com/amirhfarzaneh/disturblabel-pytorch/blob/master/README.md project
and utilizes implementation from ResNet 18 from https://github.com/huyvnphan/PyTorch_CIFAR10

**Usage**

`python main_ddl.py --mode=dl --alpha=20`


**Most important arguments:**

`--dataset` - which data to use 

Possible values:


| value | dataset |
| ------ | ------ |
|MNIST    | [MNIST](http://yann.lecun.com/exdb/mnist/)     |
|FMNIST   |[Fashion MNIST](https://github.com/zalandoresearch/fashion-mnist)           |
|CIFAR10      |[CIFAR-10](https://www.cs.toronto.edu/~kriz/cifar.html)     |
|CIFAR100  |[CIFAR-100](https://www.cs.toronto.edu/~kriz/cifar.html)                        |
|ART     |[Art Images: Drawing/Painting/Sculptures/Engravings](https://www.kaggle.com/thedownhill/art-images-drawings-painting-sculpture-engraving)         |
|INTEL    |[Intel Image Classification](https://www.kaggle.com/puneet6060/intel-image-classification)         |

Default: MNIST

`-- mode` - regularization method applied

Possible values:

| value | method |
| ------ | ------ |
|noreg    |Without any regularization    |
|dl       |Vanilla DistrubLabel          |
|ddl      |Directional DisturbLabel      |
|dropout  |Dropout                       |
|dldr     |DistrubLabel+Dropout          |
|ddldl    |Directional DL+Dropout        |

Default: ddl

`--alpha` - alpha for vanilla Distrub label and Directional DisturbLabel 

Possible values: int from 0 to 100. 
Default: 20

`--epochs` - number of training epochs

Default: 100



